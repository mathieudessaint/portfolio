import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <div className="footer">
                © Mathieu Dessaint | 2024.
            </div>
        )
    }
}

export default Footer
